import matplotlib
import seaborn as sns
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings("ignore")
from PIL import Image
import requests
from StringIO import StringIO
import numpy as np

_WHITE = '#ffffff'

_LIGHT_FACE = '#f0f0f0'
_DARK_FACE = '#595959' 

_DARK_TEXT = '#3c3c3c'
_LIGHT_TEXT = '#F7F7F7'

_LIGHT_GRID = '#e9e9e9'
_DARK_GRID = '#828282'

_LIGHT_TICKS = '#AAAAAA'
_DARK_TICKS = '#3c3c3c'

_THREE_SHADES = [  {"color":"#3c3c3c","alpha":0.8,'linestyle':'-'},
                  {"color":"#5f5f5f","alpha":0.6,'linestyle':'-'},
                  {"color":"#3c3c3c","alpha":1.0,'linestyle':'-'}]

_LOLLIPOP = [  {"color":"#5DA5DA","alpha":1,'linestyle':'-'},
              {"color":"#FAA43A","alpha":1,'linestyle':'-'},
              {"color":"#60BD68","alpha":1,'linestyle':'-'},
              {"color":"#F17CB0","alpha":1,'linestyle':'-'},
              {"color":"#B2912F","alpha":1,'linestyle':'-'},
              {"color":"#B276B2","alpha":1,'linestyle':'-'},
              {"color":"#DECF3F","alpha":1,'linestyle':'-'},
              {"color":"#F15854","alpha":1,'linestyle':'-'},
              {"color":"#4D4D4D","alpha":1,'linestyle':'-'},]


_POPTART = [    {"color":"#fff001","alpha":1,'linestyle':'-'},
              {"color":"#fd1999","alpha":1,'linestyle':'-'},
              {"color":"#99fc20","alpha":1,'linestyle':'-'},
              {"color":"#00e6fe","alpha":1,'linestyle':'-'},
              {"color":"#a10eec","alpha":1,'linestyle':'-'},]

#some defaults
footer_coordinates = [0,0,1,0.07]
main_title_font_size = 30
main_title_margin = 1.10
subtitle_font_size = 20
subtitle_margin = 1.05
image_left_margin = 70
WIDTH = 24
HEIGHT = 12
PAD = 8


class chart(object):
    
    def __init__(self, palette=None, height=HEIGHT, width=WIDTH, ):

        palette = palette or ''
        self.line_list = []
        self.label_list = []
        self.palette = palette
        
        self.width = width
        self.height = height
        self.pad = PAD
        
        if palette == 'lollipop' or palette == 'popsicle' or palette == 'sherbert':
            self.color_palette = _LOLLIPOP
            self.face_color = _LIGHT_FACE
            self.text_color = _DARK_TEXT
            self.grid_color = _LIGHT_GRID
            self.tick_color = _DARK_TICKS
        elif palette == 'poptart':
            self.color_palette = _POPTART
            self.face_color = _DARK_FACE
            self.text_color = _LIGHT_TEXT
            self.grid_color = _DARK_GRID
            self.tick_color = _LIGHT_TICKS
        elif palette == 'spark':
            self.color_palette = _LOLLIPOP
            self.face_color = _WHITE
            self.text_color = _DARK_TEXT
            self.grid_color = _WHITE
            self.tick_color = _DARK_TICKS
            self.width = WIDTH
            self.height = 3
            self.pad = 2
        elif 'http' in palette:
            self.color_palette = _POPTART
            self.face_color = _DARK_FACE
            self.text_color = _LIGHT_TEXT
            self.grid_color = _DARK_GRID
            self.tick_color = _LIGHT_TICKS
        else:
            self.color_palette = _THREE_SHADES
            self.face_color = _LIGHT_FACE
            self.text_color = _DARK_TEXT
            self.grid_color = _LIGHT_GRID
            self.tick_color = _DARK_TICKS
        
        self.line_num = 0
        self.color_num = self.line_num % len(self.color_palette)

        #apply seaborn styles
        self._add_seaborn_styles()

        #create figure
        self._create_figure()
        
        #create left and right y axes
        self._create_axes()
        
        if palette != 'spark':
        
            #create footer space
            self._add_footer(footer_coordinates,color=self.face_color)

            #set main title
            self.default_title = self._add_title(main_title_font_size,main_title_margin)

            #set subtitle
            self.default_subtitle = self._add_subtitle(subtitle_font_size,subtitle_margin)
        
            #set left y label
            self.default_ylabel = self._add_ylabel()
        
        self.fig.tight_layout(pad=self.pad, rect=[0,0,1,1])
        
        #if galaxy
        if 'http' in palette:
            img_url = palette
            response = requests.get(img_url)
            img = Image.open(StringIO(response.content))
            h = int(self.fig_height*self.fig.dpi)
            w = int(self.fig_width*self.fig.dpi)            
            img = img.resize((w,h), Image.ANTIALIAS)
            img = np.array(img)
            img = img.astype(np.float) / 255
            img = self.fig.figimage(img, zorder=-1000)
            self.ax1.patch.set_alpha(0.5)
            self.ax_footer.patch.set_alpha(0.0)
        
            
        
    def _create_axes(self):
        
        self.ax1 = self.fig.add_subplot(1,1,1)        
        self.ax2 = self.ax1.twinx()
        self.ax2.grid(False)
        self.ax2.set_yticklabels("",alpha=0.5)
        
        
    def _create_figure(self):
        
        self.fig_width = self.width
        self.fig_height = self.height
        self.fig = plt.figure(figsize=(self.fig_width,self.fig_height))
        self.fig.set_facecolor(self.face_color)
        
        
    def _add_seaborn_styles(self):

        sns.set_context("notebook", font_scale=1.5)
        sns.set_style({'axes.edgecolor': "r", 
                       'axes.facecolor': self.face_color, 
                       'grid.color': self.grid_color,
                       'grid.linewidth': 2,
                       'xtick.color': self.tick_color,
                       'ytick.color': self.tick_color,
                       'axes.labelcolor': self.text_color,
                       'text.color': self.text_color,})

        
    def _add_footer(self,footer_coordinates,color="#5b5e5f"):
        
        #footer
        self.ax_footer = self.fig.add_axes(footer_coordinates,zorder=0) 
        self.ax_footer.xaxis.set_visible(False)
        self.ax_footer.yaxis.set_visible(False)
        self.ax_footer.patch.set_color(color)

    
    def _add_title(self,main_title_font_size,main_title_margin,title='chart.set_title("Chart Title")'):

        title_main = self.ax1.text(0.0, main_title_margin,title, ha='left', fontweight='bold', fontsize=main_title_font_size, transform=self.ax1.transAxes,color=self.text_color)
        title_expand_top = self.ax1.text(0.0, 1.15,"a",  fontsize=20, ha='left', transform=self.ax1.transAxes,color=self.text_color,alpha=0)
        return title_main
    
    
    def set_title(self, title):
        """
        Sets the title of the chart.
        -----------------------------
        Input: 
            title: A string to be used as the title of the chart
        Output: 
            Returns None
        """
        self.default_title.set_text(title) 

    
    def _add_subtitle(self,subtitle_font_size,subtitle_margin,subtitle='chart.set_subtitle("This is where you include a description of the chart.")'):
        title_sub = self.ax1.text(0.0, subtitle_margin, subtitle,  fontsize=subtitle_font_size, ha='left', transform=self.ax1.transAxes,color=self.text_color)
        return title_sub
    
    
    def set_subtitle(self, subtitle):
        """
        Sets the subtitle of the chart.
        -----------------------------
        Input:
            subtitle: A string to be used as the subtitle of the chart
        Output: 
            Returns None
        """
        title_sub = self.default_subtitle.set_text(subtitle) 
        
    
    def set_xlabel(self, xlabel=""):
        """
        Sets the x-axis label of the chart.
        -----------------------------
        Input:
            xlabel: A string to be used as the x-axis label of the chart
        Output: 
            Returns None
        """
        x1 = self.ax1.set_xlabel(xlabel, fontweight='light')
    
    
    def _add_ylabel(self, ylabel='chart.set_ylabel("y-axis Label")',second_axis=False):
        
        ax = self.ax1
        if second_axis:
            ax = self.ax2
            if self.palette == 'spark':
                ylabel = ''
            else:
                ylabel='chart.set_ylabel("y2-axis Label", second_axis=True)'
        
        y1 = ax.set_ylabel(ylabel, fontweight='light')       
        return y1
    
    
    def set_ylabel(self, ylabel, second_axis=False):
        """
        Sets the y-axis label of the chart.
        -----------------------------
        Input:
            ylabel: A string to be used as the left y-axis label of the chart
            second_axis: A boolean determining which y-axis to use; False for left axis, True for right axis
        Output: 
            Returns None
        """
        
        default_label = self.default_ylabel
        if second_axis:
            default_label = self.default_y2label
        
        y1 = default_label.set_text(ylabel) 
      
    
    def _add_legend(self):
        self.ax1.legend(self.line_list, self.label_list, loc=2)

        
    def set_legend(self, **kwargs):
        self.ax1.legend(self.line_list, self.label_list, **kwargs)
    
        
    def annotate_line(self,x,color=None, linestyle='--', alpha=0.5, label=None,):
        """
        Creates a vertical line to help annotate the chart.
        -----------------------------
        Input: 
            x: A single value corresponsing to a location on the x-axis where the vertical line will be placed
            color: A Hex string determining line color, ex: '#afafaf'
            linestyle: '-' or '--' for solid or dashed lines respectively
            alpha: A float between 0-1 determining line opacity
            label: A string for the label legend (If label argument is not passed, it will not be included in the legend)
        Output: 
            Returns None
        """        
        color = color or self.text_color
        line = self.ax1.axvline(x=x, color=color, linestyle=linestyle, alpha=alpha, label=label)
        if label:
            self.line_list.append(line)
            self.label_list.append(label)
            self._add_legend()
    
    
    def annotate_text(self,x,y,label='label argument needed',rotation=90):
        """
        Creates a text-box to help annotate the chart.
        -----------------------------
        Input: 
            text: A string to be used as the annotated text
            x: A value corresponding to the desired text placement on the x-axis
            y: A value corresponding to the desired text placement on the y-axis
            rotation: text box rotation.
        Output: 
            Returns None
        """        
        self.ax1.annotate(label, xy=(x,y), fontsize=15, rotation=rotation)
        
        
    def _transform_tick_labels(self, ax, trans_func):
        
        axis_ticks = ax.get_yticks().tolist()
        trans_axis_ticks = trans_func(axis_ticks)
        ax.set_yticklabels(trans_axis_ticks,alpha=1)
        
    
    def transform_ylabel(self, trans_func, second_axis=False):
        """
        Formats the y-axis tick label of the chart.
        -----------------------------
        Input:
            trans_func: a function that takes the iterable y-axis labels formats each value reflect the desired tick label format.
            second_axis: A boolean determining which y-axis to use; False for left axis, True for right axis
        Output: 
            Returns None
        """
        ax = self.ax1
        if second_axis:
            ax = self.ax2
        
        self._transform_tick_labels(ax, trans_func)
    
    
    def scatter(self, x, y=None, label=None, second_axis=False,color=None,alpha=None,size=300,**additional_styling):
        """
        Adds a scatter to the chart.
        -----------------------------
        Input: 
            x: An iterable for the X dimension
            y: An iterable for the Y dimension
            label: A string for the label legend
            second_axis: A boolean determining which y-axis to use; False for left axis, True for right axis
            color: A Hex string determining line color, ex: '#afafaf'
            alpha: A float between 0-1 determining line opacity
            size: size of scatter circles
        Output: 
            Returns None
        """
        
        x, y = self._choose_data(x, y, chart_type='scatter')
        ax = self._choose_axis(second_axis)
        color, alpha, linestyle = self._choose_palette(color, alpha)
        
        data = [x, y]
        chart_styling = {'color':color, 'marker':'o', 's':size, 'alpha':alpha,}
        
        combined_styling = chart_styling.copy()
        combined_styling.update(additional_styling)
        
        line = ax.scatter(*data,**combined_styling)
        self.line_num += 1
        self.color_num = self.line_num % len(self.color_palette)

        self._format_y_axis_ticks(ax)
        if label or len(self.line_list)<=0:
            self._add_line_to_legend(line, label)
     
            
    def line(self, x, y=None, label=None, second_axis=False,color=None,alpha=None,linestyle=None,**additional_styling):
        """
        Adds a line to the chart.
        -----------------------------
        Input: 
            x: An iterable for the X dimension
            y: An iterable for the Y dimension
            label: A string for the label legend
            second_axis: A boolean determining which y-axis to use; False for left axis, True for right axis
            color: A Hex string determining line color, ex: '#afafaf'
            alpha: A float between 0-1 determining line opacity
            linestyle: '-' or '--' for solid or dashed lines respectively
        Output: 
            Returns None
        """
        
        x, y = self._choose_data(x, y, chart_type='line')
        ax = self._choose_axis(second_axis)
        color, alpha, linestyle = self._choose_palette(color, alpha, linestyle)
        
        data = [x, y]
        chart_styling = {'color':color, 'linestyle':linestyle, 'alpha':alpha, 'linewidth':3,}
        
        combined_styling = chart_styling.copy()
        combined_styling.update(additional_styling)
        
        line = ax.plot(*data,**combined_styling)
        self.line_num += 1
        self.color_num = self.line_num % len(self.color_palette)
        
        self._format_y_axis_ticks(ax)
        if label or len(self.line_list)<=0:
            self._add_line_to_legend(line[0], label)

        
    def fill_between(self,x,y_top,y_bottom=None,label=None,color=None,alpha=None,second_axis=False,**additional_styling):
        """
        Creates an 'area' by filling between two series. 
        The lines associated with these series will not be plotted.
        -----------------------------
        Input: 
            x: An iterable for the X dimension
            y_top: An iterable for the top series (Area will be filled beneath this)
            y_bottom: An iterable for the bottom series (Area will be filled above this)
            label: A string for the label legend (If label argument is not passed, it will not be included in the legend)
            second_axis: A boolean determining which y-axis to use; False for left axis, True for right axis
            color: A Hex string determining line color, ex: '#afafaf'
            alpha: A float between 0-1 determining line opacity
        Output: 
            Returns None
        """
        
        x, y_top, y_bottom = self._choose_data(x, y_top, y_bottom, chart_type='fill_between')
        ax = self._choose_axis(second_axis)
        color, alpha, linestyle = self._choose_palette(color, alpha, linestyle='-')
        
        data = [x, y_top, y_bottom]
        chart_styling = {'color':color,'alpha':alpha}
        
        combined_styling = chart_styling.copy()
        combined_styling.update(additional_styling)
        
        line = ax.fill_between(*data,**combined_styling)
        self.line_num += 1
        self.color_num = self.line_num % len(self.color_palette)
        
        if label or len(self.line_list)<=0:
            self._add_line_to_legend(plt.Rectangle((0, 0), 1, 1, fc=color, alpha=alpha), label)
        
        
    def _add_line_to_legend(self, line, label):
        if self.palette == 'spark' and label is None:
            pass
        else:
            label = label or 'label argument needed'
            self.line_list.append(line)
            self.label_list.append(label)
            self._add_legend()
        
        
    def _choose_palette(self, color, alpha, linestyle=None):
        
        color = color or self.color_palette[self.color_num]["color"]
        alpha = alpha or self.color_palette[self.color_num]["alpha"]
        linestyle = linestyle or self.color_palette[self.color_num]["linestyle"]
        
        return color, alpha, linestyle
        
        
    def _choose_axis(self, second_axis):
        
        if second_axis:
            ax = self.ax2
            self.default_y2label = self._add_ylabel(second_axis=True)
        else:
            ax = self.ax1
        
        return ax
        
        
    def _format_y_axis_ticks(self, ax):
        
        tick_diff = ax.get_yticks().tolist()[1] - ax.get_yticks().tolist()[0]
        new_ticks = [int(val) if tick_diff >= 1 else val for val in ax.get_yticks().tolist()]
        ax.set_yticklabels(new_ticks,alpha=1)
        

    def _choose_data(self, x, y1, y2=None, chart_type=None):
        
        if chart_type in ['line', 'scatter', 'bar']:
        
            if y1 is None:
                y1 = x
                x = [i for i in range(len(y1))]
                return x, y1
            else:
                return x, y1
            
        if chart_type in ['fill_between']:
    
            if y2 is None:
                y2 = y1
                y1 = x
                x = [i for i in range(len(y1))]
                return x, y1, y2
            else:
                return x, y1, y2
        
        
    def set_xlim(self,*max_min_args):
        self.ax1.set_xlim(*max_min_args)
        
        
    def set_ylim(self,*max_min_args,**kwargs):
        
        second_axis = kwargs.get('second_axis',False)
        ax = self._choose_axis(second_axis)
        self.ax1.set_ylim(*max_min_args)

        
        
    def bar(self, x, y=None, label=None, second_axis=False,color=None,alpha=None,**additional_styling):
        """
        Adds a bar to the chart.
        -----------------------------
        Input: 
            x: An iterable for the X dimension
            y: An iterable for the Y dimension
            label: A string for the label legend
            second_axis: A boolean determining which y-axis to use; False for left axis, True for right axis
            color: A Hex string determining line color, ex: '#afafaf'
            alpha: A float between 0-1 determining line opacity
        Output: 
            Returns None
        """
        
        x, y = self._choose_data(x, y, chart_type='bar')
        ax = self._choose_axis(second_axis)
        color, alpha, linestyle = self._choose_palette(color, alpha)
        
        data = [x, y]
        chart_styling = {'color':color, 'alpha':alpha,}
        
        combined_styling = chart_styling.copy()
        combined_styling.update(additional_styling)
        
        line = ax.bar(*data,**combined_styling)
        self.line_num += 1
        self.color_num = self.line_num % len(self.color_palette)
        
        self._format_y_axis_ticks(ax)
        if label or len(self.line_list)<=0:
            self._add_line_to_legend(line, label)
        
        
    def hist(self, x, label=None, bins=10, second_axis=False,color=None,alpha=None,**additional_styling):
        """
        Adds a bar to the chart.
        -----------------------------
        Input: 
            x: An iterable for the X dimension
            y: An iterable for the Y dimension
            bins: Number of bins
            label: A string for the label legend
            second_axis: A boolean determining which y-axis to use; False for left axis, True for right axis
            color: A Hex string determining line color, ex: '#afafaf'
            alpha: A float between 0-1 determining line opacity
        Output: 
            Returns None
        """
        
        ax = self._choose_axis(second_axis)
        color, alpha, linestyle = self._choose_palette(color, alpha)
        
        data = [x]
        chart_styling = {'color':color, 'alpha':alpha, 'bins':bins}
        
        combined_styling = chart_styling.copy()
        combined_styling.update(additional_styling)
        
        line = ax.hist(*data,**combined_styling)
        self.line_num += 1
        self.color_num = self.line_num % len(self.color_palette)
        
        self._format_y_axis_ticks(ax)
        if label or len(self.line_list)<=0:
            self._add_line_to_legend(line[2][0], label)
        
        
    
def plot(dataframe):

    ch = chart('popsicle')
    for col in dataframe.columns:
        ch.line(dataframe.index, dataframe[col],label=col)
    return ch






